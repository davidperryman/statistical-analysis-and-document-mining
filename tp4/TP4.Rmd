---
title: "TP4"
author: "Elliott Perryman, Jan Zavadil, Ignat Sabaev"
date: "4/4/2022"
output: html_document
---


## 1. Import and first explorations
#### (a) 
We first load the data from the text file and create an R graph structure. 

```{r, warning=F, echo=F}
options(warn=-1)
rm(list=ls())
set.seed(0)
library ( igraph , warn.conflicts = F)
```
```{r, echo=T}
dat <- read.table ("lesmis.txt", header = FALSE , sep = "\t")
misgraph <- simplify ( graph.data.frame ( dat , directed = FALSE ))
```

This graph structure is plotted using $\texttt{plot.igraph}$ function. It supports a vast variety of ways to fine tune the looks of the graph. It is possible to specify the layout of the graph using the $\texttt{layout}$ parameter by either providing the coordinates for each vertex or by using one of the predefined layouts, eg. circle, sphere or fruchterman.reingold as seen in the following plots.

We plot below 3 figures, each representing the graph of characters from Les Miserables. A line indicates that two characters are mentioned in the same chapter. We choose an undirected graph because the mention of two characters in a chapter does not imply there is a directed nature, at least when we treat the chapter as a bag of words. The strength of the line is proportional to how many times the two characters were mentioned together.

```{r, echo=F}
plot.igraph(
  misgraph, layout = layout.circle, vertex.color = "red", edge.color = "blue", 
  vertex.size = 8, vertex.frame.color = NA, vertex.label.font = 3, 
  vertex.label.cex = 0.7, vertex.label.dist = 1.5, vertex.label.color = "black", 
  edge.curved = TRUE,edge.width = 0.2, weight=NA,
  main = "Graph of characters from les Miserables - circle"
)
```

```{r, echo=F}
plot.igraph(
  misgraph, layout = layout.sphere, vertex.color = "red", edge.color = "blue",
  vertex.size = 8, vertex.frame.color = NA, vertex.label.font = 3, 
  vertex.label.cex = 0.7, vertex.label.dist = 2.5, vertex.label.color = "black",
  edge.curved = TRUE,edge.width = 0.2, weight=NA,
  main = "Graph of characters from les Miserables - sphere"
)
```

```{r, echo=F}
plot.igraph(
  misgraph, layout = layout.fruchterman.reingold, vertex.color = "red", 
  edge.color = "blue", vertex.size = 8, vertex.frame.color = NA, weight=NA,
  vertex.label.font = 3, vertex.label.cex = 0.7, vertex.label.dist = 1.5,
  vertex.label.color = "black", edge.curved = TRUE, edge.width = 0.2, 
  main = "Graph of characters from les Miserables - fruchterman.reingold",
)
```

As shown, the Fruchterman Reingold graph looks the best for spreading out the nodes, but it still clutters up the graph with names so that it is hard to read. We can exclude names for characters not mentioned more than 13 times so that we can read the central characters.

```{r, echo=F}
labels = names(V(misgraph))
labels[degree(misgraph)>=0] = NA
labels[degree(misgraph)>13] = names(V(misgraph))[degree(misgraph)>13]

plot.igraph(
  misgraph, layout = layout.fruchterman.reingold, vertex.color = "red", 
  edge.color = "blue", vertex.size = 5, vertex.frame.color = NA, 
  vertex.label.font = 3, vertex.label.cex = 1, #vertex.label.dist = 3.5,
  vertex.label.color = "black", edge.curved = TRUE, edge.width = 0.2, 
  main = "Graph of characters from les Miserables - fruchterman.reingold",
  vertex.label=labels
)
```

These names are repeated below:
```{r, include=T, echo=F}
names(V(misgraph))[degree(misgraph)>13]
```

Looking at the graph, we can see Jean Valjean in the center. He has a connection to a small community connected to the Bishop. Near him is Javert, which makes sense because Javert is following Jean Valjean throughout the story. The Thernardiers are nearby, and Gavroche, their son. Marius is farther from Jean Valjean, as he does not really interct too much with him. Fantine has her own storyline, mostly connected to Jean Valjean. 

#### b)

```{r, echo=F}
size = gsize(misgraph)
order = gorder(misgraph)
dens = edge_density(misgraph)
diam = diameter(misgraph, weights = NULL)
```


The graph in question is undirected, its size is `r size`, order `r order`, density `r dens` and diameter `r diam`. The graph is complete. We set the weight to NA.

#### c)
The following code calculates the font size as (degree + 10)/max(all degrees in graph).
```{r}
set.seed (3)
V(misgraph)$label.cex <- (degree(misgraph)+10) / max(degree(misgraph))
l <- layout_with_fr(misgraph)
```

This code sets the size of the label of a vertex depending on its degree. Its affect on the plot can be seen in the following plot.

```{r, echo=F}
plot.igraph(misgraph, layout = layout.fruchterman.reingold, vertex.size=3, vertex.color = "red", edge.color = "blue", vertex.size = 8, vertex.frame.color = NA, vertex.label.font = 3, vertex.label.dist = 1.5, vertex.label.color = "black", edge.curved = TRUE, edge.width = 0.2, main = "Graph of characters from les Miserables - fruchterman.reingold")
```

This graph has a highly connected central corridor with the main characters and characters that appear in the end of the book. As offshoots from this main cluster, there are several small communities. This graph more clearly shows the relationships in the text, as Jean Valjean is not able to be read in the center.

## 2 Community detection

### 2.1 Hierarchical agglomerative clustering

#### a)

In the beginning, all the vertices are considered to belong by themselves to their own cluster, at each step of the clustering the two "closest" clusters with regard to a certain metric are joined. The vertex-vertex, cluster-vertex and cluster-cluster metric must be defined.

#### b)
The dissimilarity and dendrogram are computed. When turning on the character names as labels, the text is too overlapping and hard to read so that it is just as inscrutable as if the labels were arbitrary indices.
```{r}
dissim = 1 - similarity(misgraph, method = "jaccard")
mishclust = hclust(as.dist(dissim))
plot(mishclust, main="Dendrogram of Les Miserables", xlab="character number");
```

#### c)

The following code cuts the cluster dendrogram from previous part so that the graph is divided into $i$ communities, where $i$ goes from 1 to 10. For each of these divisions the modularity is calculated and plotted.

```{r}
mod = c()
for (i in 1:25){
  labels = cutree(mishclust, i)
  mod[i] = modularity(x = misgraph , membership = labels)
}
plot ( mod , type ="l", main = "Modularity depending on number of communities", ylab = "Modularity", xlab = "Number of Communities")
```

Based on the plot, the best number of communities to divide the graph into is around 12. The modularity rises slightly for number of clusters going up to 16, but its increase is not significant enough to balance out higher complexity coming from higher number of communities.

#### d)

```{r, echo=F}
labels = cutree(mishclust, 12)
V(misgraph)$color = labels
plot.igraph(misgraph, layout = layout.fruchterman.reingold,  edge.color = "blue", vertex.size = 9 ,vertex.label.font = 3, vertex.label.dist = 1.5, vertex.label.color = "black", edge.curved = TRUE, edge.width = 0.2, main = "Cut Tree with 12 communities - Complete Linkage")
#edge_density(misgraph[labels[1]])
```

In this graph, some of the communities are much denser than others. For example, the Bishop's community is just the bishop with single points around it. The bishop is not a part of the community centered around him. This seems like a problem with the clustering. Jean Valjean is also not a part of the green color community, even though he is the center of it. Instead, he gets grouped with Javert. Also, the purple and single yellow community seems like it has been isolated even though the connections between these vertices and the others have low betweeness. We could maybe get rid of yellow, purple, and red communities, since they have few elements and don't really fit in.



#### e)

```{r}
plot(mishclust, labels=labels)
```



#### f)

```{r, echo=F}
mishclustAve = hclust(as.dist(dissim), method = "average")
mishclustSin = hclust(as.dist(dissim), method = "single")
modAve = c()
modSin = c()
for (i in 1:25){
  labelsAve = cutree(mishclustAve, i)
  labelsSin = cutree(mishclustSin, i)
  modAve[i] = modularity(x = misgraph , membership = labelsAve)
  modSin[i] = modularity(x = misgraph , membership = labelsSin)
}
plot (mod, type ="l",main = " Comparison of different linkage option for aglomerative clustering", ylab = "Modularity", xlab = "Number of Communities")
lines(modAve, type = "l", col = "red")
lines(modSin, type = "l", col = "blue")
legend(x = "bottomright", legend = c("Complete linkage","Average linkage","Single linkage"), col = c("black","red","blue"),pch = "|")
```

The figure above shows the modularity as a function of number of clusters for different linkages: complete, average, or single. We notice that there is a plateau of modularity around 0.3 that is reached by the average, single, then complete linkages in that order. These modularities increase afterwards, but we think that this is for making small communities.

```{r, echo=F}
labels = cutree(mishclustAve, 12)
V(misgraph)$color = labels
plot.igraph(misgraph, layout = layout.fruchterman.reingold,  edge.color = "blue", vertex.size = 9 ,vertex.label.font = 3, vertex.label.dist = 1.5, vertex.label.color = "black", edge.curved = TRUE, edge.width = 0.2, main = "Cut Tree with 12 communities - Average Linkage")
#edge_density(misgraph[labels[1]])
```
```{r, echo=F}
labels = cutree(mishclustSin, 12)
V(misgraph)$color = labels
plot.igraph(misgraph, layout = layout.fruchterman.reingold,  edge.color = "blue", vertex.size = 9 ,vertex.label.font = 3, vertex.label.dist = 1.5, vertex.label.color = "black", edge.curved = TRUE, edge.width = 0.2, main = "Cut Tree with 12 communities - Single Linkage")
#edge_density(misgraph[labels[1]])
```

Looking at the figures above, we can see that the divisions still seem to prioritize cutting small communities away, rather than dividing up some of the larger communities. This makes it so that there are some single vertex points that don't really make sense to call a community.


## 2.2 Edge betweenness

#### a)
We introduce the term of edge betweenness which is for each edge defined as the number of shortest paths between a pair of nodes that is passing through that edge. This metric favors the edges between two communities since the vertices from two different clusters are not as strongly connected and so the shortest path between them must go through a choke point, which can then be removed in a divisive hierarchical clustering algorithm.

#### b) 

```{r}
mis_edgeb = cluster_edge_betweenness(misgraph)
plot(as.dendrogram(mis_edgeb), main = "Dendrogram of the Edge Betweenness divisive clustering")
print(mis_edgeb)
```
```{r}
plot(mis_edgeb,misgraph)
```



#### c)

The following code cuts the edges connecting the communities determined by the edge betweenness. This makes the different communities disconnected in the following graph.
```{r}
f <- function (i){
mis_graph2 = delete.edges(misgraph, mis_edgeb$removed.edges[seq(length = i)])
cl = clusters(mis_graph2)$membership
modularity(misgraph, cl)
}
mods = sapply (0:ecount(misgraph), f)
mis_graph2 <- delete.edges(misgraph, mis_edgeb$removed.edges[seq(length = which.max(mods) - 1)])

plot(mis_graph2)
```

## 2.3 Spectral clustering and the Louvain algorithm
The Louvain clustering is shown below.
```{r}
misLouvain = cluster_louvain(misgraph)
print(misLouvain)
plot(misLouvain,misgraph, main="Louvain Clustering")
```

This method found 6 communities. This seems like a better job than the others since there are fewer isolated groups and no groups caused just by a chain.

```{r}
misLeadingeigen = cluster_leading_eigen(misgraph)
print(misLeadingeigen)
plot(misLeadingeigen,misgraph,main="Leading Eigen clustering")
```

Leading eigen found 8 communities.


## 2.4 Conclusion

When we consider this lab, one of the main takeaways from it is the difficulty in visualizing the data. Even after adjusting the size of the font for the importance of the character, it is still difficult to read the graphs. This simple example has some important conclusions. Only because I read the book do I know what names to look for as central characters. Without this, it would be harder to interpret some of the effects that are happening, like that there is more mixing between characters towards the end of the book, so characters at the end of the book are more interconnected. Even for this relatively small example, reading and interpreting the graph is already complex just because of data visualization problems. When almost all our decisions about what to do are based on looking at the graph, the importance of the graph visualization cannot be overstated.
The other effect we noticed was that the agglomerative algorithms left many single or few point communities. This is not what we want, and even when we accept lower modularity, we see that there are some of these small communities.


